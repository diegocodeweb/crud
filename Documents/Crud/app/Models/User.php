<?php namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table      = 'users';
    public    $timestamps = false;
    protected $primaryKey = 'cedula';

    protected $User = [
        'cedula', 
        'firstname',
        'secondname',
        'firstlastname',
        'secondlastname'
    ];
    
    protected $fillable = [
        'cedula', 
        'firstname',
        'secondname',
        'firstlastname',
        'secondlastname'
    ];
}
