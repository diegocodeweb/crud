<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\CrudController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\User;
use DB;


class CrudController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function storeUser (Request $request) {
        try {
            $cedula = $request->input('cedula');
            $first_name = $request->input('firstname');
            $second_name = $request->input('secondname');
            $first_lastname = $request->input('firstlastname');
            $second_lastname = $request->input('secondlastname');

            $user = User::find($cedula);

            if($user) {
                return response()->json(['Status' => 'false', 'Message' => 'El usuario ya existe!'], 200);
            } else {
                $users = new User([
                    'cedula' => $cedula,
                    'firstname' => $first_name,
                    'secondname' => $second_name,
                    'firstlastname' => $first_lastname,
                    'secondlastname' => $second_lastname,
                ]);
                $users->save();
                return response()->json(['Status' => 'Succeed', 'Message' => 'El usuario ha sido creado satisfactoriamente!'], 200);
            }        
            
        } catch (\QueryException $ex) {
            $dataObject = array('Code' => '99', 'Status' => 'False', 'Message' => 'QueryException.!' . $ex->getMessage());
            Log::critical($ex);
            return response()->json($dataObject, 200);
        } catch (\Exception $ex) {
            Log::critical($ex);
            $dataObject = array('Code' => '99', 'Status' => 'False', 'Message' => 'Exception.!' . $ex->getMessage());
            return response()->json($dataObject, 200);
        }
    }
    public function deleteUsers(Request $request, $cedula) {
        try {
            Log::debug($cedula);
            $user = User::find($cedula);

            if($user)  {
                $query = DB::delete('call proc_deleteUser(?)',array($cedula));
                return response()->json(['Status' => 'Succeed', 'Message' => 'El usuario ha sido borrado satisfactoriamente!'], 200);
            } else {
                return response()->json(['Status' => 'false', 'Message' => 'El usuario que intenta borrar ya ha sido borrado o no existe!'], 200);
            }
            
        } catch (\QueryException $ex) {
            $dataObject = array('Code' => '99', 'Status' => 'False', 'Message' => 'QueryException.!' . $ex->getMessage());
            Log::critical($ex);
            return response()->json($dataObject, 200);
        } catch (\Exception $ex) {
            Log::critical($ex);
            $dataObject = array('Code' => '99', 'Status' => 'False', 'Message' => 'Exception.!' . $ex->getMessage());
            return response()->json($dataObject, 200);
        }
    }

    public function updateUser(Request $request) {
        try{
            $cedula = $request->input('cedula');
            $first_name = $request->input('firstname');
            $second_name = $request->input('secondname');
            $first_lastname = $request->input('firstlastname');
            $second_lastname = $request->input('secondlastname');

            $query = DB::statement('call proc_updateUser(?,?,?,?,?)', array($cedula,$first_name, $second_name, $first_lastname, $second_lastname));
            return response()->json(['Status' => 'true', 'Message' => 'Producto actualizado exitosamente.'],200);

        }catch (\QueryException $ex) {
            $dataObject = array('Code' => '99', 'Status' => 'False', 'Message' => 'QueryException.!' . $ex->getMessage());
            Log::critical($ex);
            return response()->json($dataObject, 200);
        } catch (\Exception $ex) {
            Log::critical($ex);
            $dataObject = array('Code' => '99', 'Status' => 'False', 'Message' => 'Exception.!' . $ex->getMessage());
            return response()->json($dataObject, 200);
        }
    }

    public function getUsers(Request $request) {
        try {
            $query = DB::select('call proc_getUsers()');
            $dataObject = response()->json($query);
            return $dataObject;
        } catch (\QueryException $ex) {
            $dataObject = array('Code' => '99', 'Status' => 'False', 'Message' => 'QueryException.!' . $ex->getMessage());
            Log::critical($ex);
            return response()->json($dataObject, 200);
        } catch (\Exception $ex) {
            Log::critical($ex);
            $dataObject = array('Code' => '99', 'Status' => 'False', 'Message' => 'Exception.!' . $ex->getMessage());
            return response()->json($dataObject, 200);
        }
    }

    public function getUserxId(Request $request, $cedula) {
        try {
            Log::debug($cedula);
            $users = DB::select('call proc_userById(?)',array($cedula));
            $users = (object) $users;
            return response()->json( $users, 200);
        } catch (\QueryException $ex) {
            $dataObject = array('Code' => '99', 'Status' => 'False', 'Message' => 'QueryException.!' . $ex->getMessage());
            Log::critical($ex);
            return response()->json($dataObject, 200);
        } catch (\Exception $ex) {
            Log::critical($ex);
            $dataObject = array('Code' => '99', 'Status' => 'False', 'Message' => 'Exception.!' . $ex->getMessage());
            return response()->json($dataObject, 200);
        }
    }


}
