<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function () {
    //Metodo que accede a la API para crear usuarios 
    Route::post('/createUser', 'CrudController@storeUser');
    //Metodo que accede a la API para actualizar un usuario
    Route::put('/updateUser', ['as' => 'editUser', 'uses' => 'CrudController@updateUser']);
    //Metodo que accede a la API para traer todos los usuarios
    Route::get('/listUser', ['as' => 'listarUsuario', 'uses' => 'CrudController@getUsers']);
    //Metodo que accede a la API para traer un usuario por ID 
    Route::get('/getUserById/{cedula}', 'CrudController@getUserxId');
    //Metodo que accede a la API para eliminar un usuario por Cedula
    Route::delete('/deleteUser/{cedula}', 'CrudController@deleteUsers');
}
);
