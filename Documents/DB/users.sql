CREATE TABLE `users` (
  `cedula` int(15) NOT NULL,
  `firstname` varchar(30) DEFAULT NULL,
  `secondname` varchar(30) DEFAULT NULL,
  `firstlastname` varchar(30) NOT NULL,
  `secondlastname` varchar(30) NOT NULL,
  PRIMARY KEY (`cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
