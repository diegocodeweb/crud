DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_userById`(IN _cedula integer(20))
BEGIN
	select cedula,firstname,secondname,firstlastname,secondlastname
		from users where _cedula = cedula;
END$$
DELIMITER ;
