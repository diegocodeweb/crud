DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_deleteUser`(IN _cedula integer(20))
BEGIN
	delete from users where cedula = _cedula;
END$$
DELIMITER ;
