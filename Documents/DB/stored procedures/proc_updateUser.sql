DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_updateUser`(IN _cedula integer(20), IN _firstname varchar(30), IN _secondname varchar(30), IN _firstlastname varchar(30), IN _secondlastname varchar(30))
BEGIN
	update users SET cedula = _cedula,firstname = _firstname,secondname = _secondname,firstlastname = _firstlastname,secondlastname = _secondlastname
     where cedula = _cedula;
END$$
DELIMITER ;
