import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppSettings } from '../app.settings';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }


  getAllUsers() {
    return this.http.get(AppSettings.API_ENDPOINT + 'listUser');
  }

  create(user: any) {
    console.log(user);
    return this.http.post(AppSettings.API_ENDPOINT + 'createUser', user);
  }

  deleteUser(cedula) {
    console.log(cedula);
    return this.http.delete(`${AppSettings.API_ENDPOINT}deleteUser/${cedula}`);
  }

  getUserById(cedula) {
    console.log(cedula);
    return this.http.get(`${AppSettings.API_ENDPOINT}getUserById/${cedula}`);
  }
  updateUser(users) {
    return this.http.put(AppSettings.API_ENDPOINT + 'updateUser', users);
  }

}
