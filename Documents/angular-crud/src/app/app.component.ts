import { Component } from '@angular/core';
import { User } from './models/user';
import { UserService } from '../../src/app/services/user.service';
import { FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  bTipo: boolean;
  verUsers: boolean;
  hideRegister: boolean;
  btnBack: boolean;
  message: string;
  value: any;
  user: any;
  name: any;
  myForm: FormGroup;
  iditem: null;
  users: any;
  cedula: number;
  cedulaxUser: number;
  firstname: any;
  secondname: any;
  alluser: any;
  firstlastname: any;
  secondlastname: any;
  headElements = ['cedula', 'FirstName', 'SecondName', 'FirstlastName', 'SecondlastName'];

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
  ) {
    this.Form();
    this.bTipo = false;
    this.verUsers = false;
    this.hideRegister = false;
    this.btnBack = false;
  }
  selectedUser: User = new User();
  saveUser(user: any) {
    console.log(this.selectedUser);
    this.userService.create(this.selectedUser)
      .subscribe(data => {
        console.log(data);
        if (data['Status'] === 'false') {
          alert('El usuario ya se encuentra registrado en la base de datos');
        } else {
          alert('El usuario se ha creado satisfactoriamente');
        }
      });
  }
  editUser() {

    this.userService.updateUser(this.selectedUser)
      .subscribe(data => {
        console.log(data);
        if (data['Status'] === 'false') {
          alert('Error al actualizar el usuario');
          window.location.reload();
        } else {
          alert('Usuario actualizado satisfactoriamente !');
          window.location.reload();
        }
      });
  }
  back() {
    this.verUsers = false;
    this.hideRegister = false;
  }
  deleteUser() {
    this.alluser = this.userService.getAllUsers().subscribe((data: any) => {
      this.cedula = data[0].cedula;
      this.userService.deleteUser(this.cedula).subscribe((res: any) => {
        console.log(res);
        if (data['Status'] === 'false') {
          alert('El usuario que intenta borrar ya ha sido borrado o no existe!');
        } else {
          alert('El usuario ha sido borrado satisfactoriamente!');
        }
      });
    });
  }
  SelectedUser() {

    this.alluser = this.userService.getAllUsers().subscribe((data: any) => {
      for (let i = 0; i <= data.length; i++) {
        this.value = data[i].cedula;
        this.myForm.patchValue({ item: this.value });
        console.log(i, this.value);
      }
    });
  }
  getAllUsers() {
    this.hideRegister = true;
    this.verUsers = true;
    this.alluser = this.userService.getAllUsers().subscribe(data => {
      console.log(data);
      this.users = data;
      this.cedula = data[0].cedula;
      this.firstname = data[0].firstname;
      this.secondname = data[0].secondname;
      this.firstlastname = data[0].firstlastname;
      this.secondlastname = data[0].secondlastname;
      console.log(this.users);
    });
  }
  private Form() {
    return this.myForm = this.fb.group({
      cedula: [''],
      firstname: [''],
      secondname: [''],
      firstlastname: [''],
      secondlastname: [''],
    });
  }

}
